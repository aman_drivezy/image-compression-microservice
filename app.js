'use strict';
const express    = require('express'); //Initialising the framework
const app        = express();
const bodyParser = require('body-parser');
const yamljs     = require('yamljs');

global.config   = yamljs.load(process.argv[2] || './config.yml');
global.Instance = config.instance;   //Declaring global variables

const consumer   = require('./lib/aws-queue-consumer');
const api        = require('./api/index');
const PORT       = config[Instance].express.port || 50010;
const firebaseDb = require('./lib/firebase-wrapper');
const utility    = require('./lib/utility');


//importing sentry library and Initialization of the required
const { Logger } = require('./lib/logger');
let logger       = new Logger(config[Instance].sentry);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', api);
consumer.start();
utility.scheduleRestartJob();

/**
 * Loggs the Exception
 */
process.on('uncaughtException', function (err) {
    console.error(err.stack);
    console.info("Node NOT Exiting...");
});


app.listen(PORT, () => {
    logger.info(`app is listening on port ${PORT}`);
});
