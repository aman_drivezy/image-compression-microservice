'use strict';

const fs               = require('fs');
const got              = require('got');
const imagemin         = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg  = require('imagemin-mozjpeg');
const { Logger }       = require('./logger');
const rp               = require('request-promise');
const scheduler        = require('node-cron');

let logger = new Logger(config.sentry);


exports.imageCompressor = (inputFile, callback) => {
    got.get(inputFile, { encoding: null })
        .then(imageResponse => {

            const quality = 80;
            imagemin.buffer(imageResponse.body, {
                plugins: [
                    imageminMozjpeg(),
                    imageminJpegtran({ progressive: true }),
                    imageminPngquant({ quality: `${quality}-80` })
                ]
            }).then(compressedImage => {
                if (callback) callback(compressedImage);
            }).catch(err => {
                logger.error(err.message);
            });
        }).catch(err => {
        logger.error(err);
    });
}

exports.sendUploadedImages = (requestBody, callback = null) => {
    var options = {
        method: 'POST',
        uri   : config[Instance].jrAppUrl + '/uploadedFirebaseImages',
        body  : { "documents": requestBody },
        json  : true
    };

    rp(options)
        .then((res) => {
            if (res.success) {
                let nodeArray = [];

                res.response.forEach((key) => {
                    let nodeLink = 'ImagesBucket/' + key['source'] + '_' + key['source_id'] + '/' + key['node_name'];
                    nodeArray.push(nodeLink);
                });

                if (typeof callback == "function") {
                    logger.info(`Deleting nodes ${nodeArray}`);
                    callback(nodeArray);
                }
            }
        })
        .catch((err) => {
            logger.error(`JR API responded with an error. ${err}`);
        });
}

exports.isInstanceProduction = () => {
    if (Instance === "DEV") return false;
    return true;
}

exports.getConfig = () => {
    return config[Instance];
}

exports.scheduleRestartJob = () => {
   scheduler.schedule('*/30 * * * *',()=>{
       logger.info('restarting job triggered');
       process.exit(0);
   });
}

