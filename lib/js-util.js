'use strict';

exports.isUndefined = (v) => {
    return (typeof v === 'undefined');
}

/**
 * States if a variable is to be considered null or not.
 *
 * @type {function(*=)}
 */
let isNull = exports.isNull = (value) => !(typeof (value) !== 'undefined' && value);

/**
 * States if a variable is to be considered null or not.
 *
 * @type {function(*=)}
 */
exports.isNotNull = (value) => !isNull(value);