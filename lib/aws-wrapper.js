'use strict';

const utility = require('./utility');   //getting the configuration
let config    = utility.getConfig();

const AWS        = require('aws-sdk');
const jsUtil     = require('./js-util');
const consumer   = require('sqs-consumer');
const { Logger } = require('./logger');
const awsConf    = config.aws;
const queueUrl   = config.queueUrl;

// AWS.config.loadFromPath('./lib/config.js');
let logger = new Logger(config.sentry);

//Creating Aws S3 instance to connect with
const s3 = new AWS.S3({
    accessKeyId    : config.s3.accessKeyId,
    secretAccessKey: config.s3.secretAccessKey,
    Bucket         : config.s3.bucketName,
});

const SQS = new AWS.SQS(awsConf);

/**
 * Returns uploaded file path in the callback.
 * First compress and then upload image to S3.
 *
 * @param {string] fileName The Url or path of the file to be compressed and uploaded to S3.
 * @param {function} callback. Returns s3 path in the parameter of the function.
 */
exports.uploadToS3 = (fileName, callback) => {
    logger.info('uploadToS3');

    let params = {
        Bucket         : config.s3.bucketName,
        ACL            : 'public-read',
        ContentEncoding: 'base64',
        ContentType    : 'image/jpeg'
    }

    let index    = fileName.lastIndexOf('.');
    let fileType = fileName.substring(index, fileName.length);

    params.Key = fileName.substring(0, index) + '_thumbnail' + fileType;

    utility.imageCompressor(fileName, (compressedData) => {
        params.Body = compressedData;

        s3.upload(params, (err, data) => {
            logger.info(`Uploading compressed image. Key: ${params.Key}`);

            if (err) {
                if (typeof callback === 'function') return callback({ err: 1, response: err });
            }

            if (typeof callback === 'function') return callback({ err: 0, response: data });
        });

    });
}

/**
 * Creates the new SQS.
 *
 * @param {function} callback Take the url of the created queue in the parameter.
 */
exports.createSqs = (callback) => {
    const params = {
        QueueName : 'testQueue',
        Attributes: {
            DelaySeconds          : '60',
            MessageRetentionPeriod: '86400'
        }
    }
    SQS.createQueue(params, (err, data) => {
        if (err) {
            logger.error(err);
            if (typeof callback === 'function') return callback({ err: 1, response: err });
        } else {
            if (typeof callback === 'function') return callback({ err: 0, response: data });
        }

    });
}

/**
 * Get the all available queue for an AWS configuration.
 *
 * @param {function} callback Take the url of all the available queue in the parameter.
 */
exports.listQueue = (callback) => {
    SQS.listQueues(function (err, data) {
        if (err) {
            logger.error(err);
            if (typeof callback === 'function') return callback({ err: 1, response: err });
        }
        else {
            logger.info('success');
            logger.info(data);
            if (typeof callback === 'function') return callback({ err: 0, response: data });
        }
    });
}

/**
 * Send Message on the SQS.
 *
 * @param {string} message Message to be sent on SQS.
 * @param {function} callback Take the response of message sent to the queue in the parameter.
 */
exports.sendQueueMessage = (message, callback) => {
    const params = {
        MessageBody : message,
        QueueUrl    : queueUrl,
        DelaySeconds: 0
    };

    SQS.sendMessage(params, function (err, data) {

        if (err) {
            logger.error(err);
            if (typeof callback === 'function') return callback({ err: 1, response: err });
        } else {
            if (typeof callback === 'function') return callback({ err: 0, response: data });
        }
    });
}

/**
 * Creates a new SQS listener and listens for
 * messages on the specified queue.
 *
 * @param config AWS configuration
 * @constructor
 */
class Listener {

    /**
     * Initializes SQS client from the given configuration.
     *
     * @param config AWS SQS configuration
     */
    constructor(config) {
        this.config = config;
        this.sqs    = SQS
    }

    /**
     * Listens for messages on a specified queue.
     *
     * @param url SQS queue URL
     * @param handler a callback function
     */
    listen(url, handler) {
        // noinspection JSUnusedGlobalSymbols
        let client = consumer.create({
            sqs              : new AWS.SQS(awsConf),
            queueUrl         : queueUrl,
            batchSize        : 10,
            visibilityTimeout: 30,
            handleMessage    : (message, done) => {
                if (typeof handler === 'function') handler(null, message);
                done();
            }
        });

        client.on('error', (err) => {
            handler(err);
        });

        client.start();
    }
}

exports.Listener = Listener;

