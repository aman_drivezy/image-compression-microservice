'use strict';

/**
 * Returns the Date after getting the past time.
 * @param {integer} minutes The minutes that are to be deducted from the required time
 * @param {string} date The Date passed in string with 'YYYY-MM-DD HH:MM:SS' format from which time should be deducted
 * @returns {Date} Date return after deducting the required minutes
 */
exports.getPastTime = function (minutes, date = null) {
    return date ? new Date(new Date(date).getTime() - minutes * 60000) : new Date(new Date().getTime() - minutes * 60000);
}