'use strict';

const moment = require('moment');
const raven = require('raven');

const jsutils = require('./js-util');

/**
 * Provides functions to log messages to either STDOUT or STDERR.
 *
 * @type {exports.Logger}
 */
class Logger {

    /**
     * Initializes Raven client if a valid Sentry API URL
     * is provided.
     *
     * @param {string=} sentry API URL
     */
    constructor(sentry) {
        if (jsutils.isNotNull(sentry)) {
            this.client = raven.Client(sentry);
        }

        this.isVerbose = false;
    }

    /**
     * Sets isVerbose flag state.
     *
     * @param {boolean} verbose flag state
     */
    setVerbose(verbose) {
        this.isVerbose = verbose;
    }

    /**
     * Prints message if verbose mode is on.
     *
     * @param {*} msg info message
     */
    verbose(msg) {
        if (this.isVerbose) this.info(msg);
    }

    /**
     * Prints the message to standard output stream.
     *
     * @param {*} msg info message
     */
    info (msg) {
        let date = moment().format('YYYY-MM-DD H:mm:ss');

        if (Array.isArray(msg)) {
            for (let i in arguments) {
                console.log(`[${date}] INFO: ${arguments[i]}`);
            }

        } else {
            console.log(`[${date}] INFO: ${msg}`);
        }
    }

    /**
     * Prints the message to standard error stream.
     *
     * @param {Error|string|{message: String}} err error message
     */
    error (err) {
        let date = moment().format('YYYY-MM-DD H:mm:ss');
        let reason = String(err.message || err);

        if (reason.indexOf('[WARN]') > -1) {
            reason = reason.replace('[WARN]', '');
            console.error(`[${date}] WARN: ${reason}`);
            return;

        } else {
            console.error(`[${date}] ERROR: ${reason}`);
        }

        if (typeof this.client !== 'undefined' && jsutils.isNotNull(err)) {
            this.client.captureException(err);
        }
    }
}

exports.Logger = Logger;