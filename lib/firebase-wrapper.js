'use strict';

const utility = require('./utility');   //getting the configuration
let config    = utility.getConfig();

const firebaseWrapper = require('firebase-admin');
const firebaseConf    = require('../firebase');
const AWS             = require('aws-sdk');
const { Logger }      = require('./logger');
const jsUtil          = require('./js-util');

// Get a reference to the database service
firebaseWrapper.initializeApp({
    credential : firebaseWrapper.credential.cert(firebaseConf),
    databaseURL: config.firebase.databaseUrl
});

let logger = new Logger(config.sentry); //initialising sentry.

//initialising s3.
const S3 = new AWS.S3({
    accessKeyId    : config.s3.accessKeyId,
    secretAccessKey: config.s3.secretAccessKey,
    Bucket         : config.s3.bucketName,
});

const db = firebaseWrapper.database();

let ref = db.ref("ImagesBucket"); // Adding the reference in the database.

ref.once("value", function (snapshot) {
    //console.log("value of snapshot ", snapshot.val());
});


/**
 * Retrieve new posts as they are added to our database
 */
ref.on("child_added", function (snapshot) {
    let newNode = snapshot.val();
    let name    = snapshot.key;
    //calling process node function to process the node.

    processNode(newNode, name, (data) => {
        if (data.length <= 0) {
            logger.info(`Invalid data length from node ${name}`);
            return;
        }

        utility.sendUploadedImages(data, (nodeData = []) => {
            if (nodeData.length <= 0) {
                return;
            }

            nodeData.forEach((key) => {
                let refr = db.ref(key);

                refr.remove()
                    .then(() => {
                        logger.info(`Deleted node ${key}.`);

                    }).catch((err) => {
                    logger.error(`Failed to delete node ${key}. ${err}`);
                });
            });
        });

    });
});

exports.removeChildNode = (node) => {
    ref.remove(node);
}


/**
 * Returns an array of object containing node value with
 *
 * @param {object} nodeVal the node passed as parameter.
 * @param {string} nodeParentName Parent of the node passed.
 * @param {function} callback function with an array of object passed as a parameter.
 */
let processNode = function (nodeVal, nodeParentName, callback) {
    let nodeValKeys = Object.keys(nodeVal);

    let imageObjArr = [];
    let i           = 0;

    /**
     * Upload's the image to s3 synchronously
     *
     * on the basis of keys calling a recursive function returning the callback
     * @param {integer} count.
     * @param {object} newNodeVal Node object to be processed.
     * @param {string} newNodeParentName Name of the parent of the node.
     * @param {array} newNodeKeys The name of the subnodes.
     * @returns {*}
     */
    let uploadImage = function (count, newNodeVal, newNodeParentName, newNodeKeys) {
        let key = newNodeKeys[count];

        /* If parentName is undefined then return. */
        if (jsUtil.isUndefined(newNodeParentName) || newNodeParentName == 'undefined') return;

        if (!newNodeVal[key].is_url) {
            let newNodeSourceInfo = newNodeParentName.split('_');
            let newNodeSource     = newNodeSourceInfo[0];
            let newNodeSourceId   = newNodeSourceInfo[1];

            if (jsUtil.isUndefined(newNodeSource) || jsUtil.isUndefined(newNodeSourceId)  //If node source or source id is undefined then return
                || newNodeSource === 'undefined' || newNodeSourceId === 'undefined') {

                if (i === nodeValKeys.length - 1) return callback(imageObjArr);

                i++;
                uploadImage(i, nodeVal, nodeParentName, newNodeKeys);

            } else {
                let params = {
                    Bucket         : config.s3.bucketName,
                    ACL            : 'public-read',
                    Body           : new Buffer(newNodeVal[key].file.replace(/^data:image\/\w+;base64,/, ""), 'base64'),
                    Key            : newNodeParentName + key + ".jpg",
                    ContentEncoding: 'base64',
                    ContentType    : 'image/jpeg'
                }

                //upload the file to s3
                S3.upload(params, (err, data) => {

                    if (err) {
                        logger.error(`Failed to upload to S3. ${err}`);
                        return;
                    }

                    imageObjArr.push({
                        document_link: data.Location,
                        source       : newNodeSource,
                        source_id    : parseInt(newNodeSourceId),
                        document_type: newNodeVal[key].type,
                        node_name    : key,
                        created_by   : jsUtil.isUndefined(newNodeVal[key]["created_by"]) ? 3 : newNodeVal[key]["created_by"]
                    });

                    if (i === nodeValKeys.length - 1) return callback(imageObjArr);

                    i++;
                    uploadImage(i, nodeVal, nodeParentName, newNodeKeys); //calling the function recursively
                });
            }
        } else {
            if (i === nodeValKeys.length - 1) {
                return callback(imageObjArr);
            }

            i++;
            uploadImage(i, nodeVal, nodeParentName, newNodeKeys); //calling the function recursively
        }
    }

    uploadImage(i, nodeVal, nodeParentName, nodeValKeys);
}

module.exports = db;