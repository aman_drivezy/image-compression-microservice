'use strict';

const utility = require('./utility');   //getting the configuration
let config    = utility.getConfig();

const { Listener, uploadToS3 } = require('./aws-wrapper');
const { Logger }               = require('./logger');
const awsConf                  = config.aws;
const queueUrl                 = config.queueUrl;

let logger = new Logger(config.sentry);

/**
 * Starts the listeners on AWS queues and consumes the data.
 */
exports.start = () => {
    logger.info(`Consuming from queue ${queueUrl}`);

    let listener = new Listener(awsConf);

    listener.listen(queueUrl, (err, message) => {
        if (err) {
            logger.error(`Could not consume SQS data: ${err}`);
            return;
        }

        let messageBody = message.Body;

        logger.info(`Data polled from SQS ${messageBody}.`);

        // upload the image to the s3
        uploadToS3(messageBody, (uploadData) => {
            logger.info(`Uploaded to S3 ${uploadData}.`);

            if (uploadData.err) {
                logger.error(`Error while uploading compressed image. ${uploadData.err}`);
            }
        });
    });
};