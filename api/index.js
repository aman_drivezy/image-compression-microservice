'use strict';

const utility = require('../lib/utility');   //getting the configuration
let config    = utility.getConfig();

const express      = require('express');
const jsUtil       = require('../lib/js-util');
const awsFunctions = require('../lib/aws-wrapper');
const { Logger }   = require('../lib/logger');

let app    = express.Router();
let logger = new Logger(config.sentry);

app.get('/image/upload', (req, res) => {
    const { file } = req.query;
    if (jsUtil.isUndefined(file)) return res.json({ success: false, response: 'File is missing' });
    utility.imageCompressor(file);
    return res.json({ success: true, response: 'Parameters are missing' });
});

app.get('/sqs/create', (req, res) => {
    awsFunctions.createSqs((data) => {
        return res.json({ success: true, response: `queue is started : ${data.response}` });
    });

});

app.get('/sqs/list', (req, res) => {
    awsFunctions.listQueue((data) => {
        return res.json({ success: true, response: `queue list is : ${data.response}` });
    });
});

app.get('/sqs/received', (req, res) => {
    awsFunctions.receiveQueueMessage((data) => {
        logger.info('the data received from sqs is ', data);
    });
    res.json({ success: true, response: `data received from queue is : ` });
});

app.post('/sqs/send', (req, res) => {
    logger.info('endpoint hit');
    let { message } = req.body;
    if (jsUtil.isUndefined(message)) return res.json({ success: false, response: 'File is missing' });

    awsFunctions.sendQueueMessage(message, function (data) {
        logger.info('Sent Message', data.response);
        res.json({ success: true, response: `File sent to sqs : ${data.response}` });
    });
});

module.exports = app;